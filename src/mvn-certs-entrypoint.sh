#!/bin/sh -eu
cp /certs/*.crt /usr/local/share/ca-certificates/
update-ca-certificates
keytool -import  -trustcacerts -file /usr/local/share/ca-certificates/*.crt -keystore /opt/java/openjdk/lib/security/cacerts -storepass changeit -noprompt

/usr/local/bin/mvn-entrypoint.sh "$@"