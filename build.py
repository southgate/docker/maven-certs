#!/usr/bin/env python3
from common import *


def main():
    tags_by_digest, digest_by_tag = fetch_tags("maven")
    primary_tag, other_tags = determine_alias_tags_matching_tag("3", "amd64", tags_by_digest, digest_by_tag)
    build_files({
        "maven_tag": primary_tag
    })
    build_image(primary_tag, other_tags, "davidsouthgate/maven-certs")
    push_repo_images("davidsouthgate/maven-certs")


if __name__ == '__main__':
    main()
