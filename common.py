import os
import shutil
import subprocess
import sys
import urllib.parse
from string import Template

import requests


def fetch_tags(repository, url=None, tags_by_digest=None, digest_by_tag=None):
    print("Fetching and processing tags")
    if url is None:
        url = "https://registry.hub.docker.com/v2/repositories/library/" + repository + "/tags?" \
              + urllib.parse.urlencode({"page": 1, "page_size": 100})
    if tags_by_digest is None:
        tags_by_digest = {}
    if digest_by_tag is None:
        digest_by_tag = {}

    print("Fetching tags from " + url)
    response = requests.get(url)
    if response.status_code != 200:
        print("Error: Request to registry returned status {} for {}".format(response.status_code, url))
        sys.exit(1)
    json = response.json()

    for tag in json["results"]:
        name = tag["name"]
        for image in tag["images"]:
            architecture = image["architecture"]
            digest = image["digest"]
            if digest not in tags_by_digest:
                tags_by_digest[digest] = []
            if name not in digest_by_tag:
                digest_by_tag[name] = {}
            if architecture not in digest_by_tag[name]:
                digest_by_tag[name][architecture] = []
            tags_by_digest[digest].append(name)

            # NOTE: This assumes the arch is unique within the set of images which may not be true
            digest_by_tag[name][architecture] = digest

    next_url = response.json()["next"]
    if next_url is not None:
        return fetch_tags(repository, next_url, tags_by_digest, digest_by_tag)
    else:
        return tags_by_digest, digest_by_tag


def determine_alias_tags_matching_tag(tag, arch, tags_by_digest, digest_by_tag):
    digest = digest_by_tag[tag][arch]
    tags = tags_by_digest[digest]
    primary_tag = None
    for tag in tags:
        if primary_tag is None:
            primary_tag = tag
        elif len(tag) > len(primary_tag):
            primary_tag = tag
    if primary_tag is None:
        print("Error: Could not determine most specific tag")
        sys.exit(1)
    other_tags = tags
    other_tags.remove(primary_tag)
    print("Using primary tag {} and other tags: {}".format(primary_tag, other_tags))
    return primary_tag, other_tags


def build_files(vars):
    print("Building files using {}".format(vars))
    path = "./build"
    if os.path.exists(path):
        shutil.rmtree(path)
    shutil.copytree("./src", "./build")

    with open("build/Dockerfile", "r") as file:
        template = Template(file.read())

    with open("build/Dockerfile", "w") as file:
        result = template.substitute(vars)
        file.write(result)


def build_image(primary_tag, other_tags, repo):
    print("Building image {}:{}".format(repo, primary_tag))
    command = docker_build_command(primary_tag, other_tags, repo)
    process = subprocess.Popen(command)
    process.wait()
    if process.poll() != 0:
        print("Error: Could not build image")
        sys.exit(1)


def docker_build_command(primary_tag, other_tags, repo):
    command = ["docker", "build", "--rm", "--tag", repo + ":" + primary_tag]
    for tag in other_tags:
        command.append("--tag")
        command.append(repo + ":" + tag)
    command.append("./build")
    return command


def push_repo_images(repo):
    print("Pushing images in repo {}".format(repo))
    process = subprocess.Popen(["docker", "push", repo, "--all-tags"])
    process.wait()
    if process.poll() != 0:
        print("Error: Could not push images")
        sys.exit(1)
